from pwn import *

class Bank(object):
    def __init__(self, p, debug):
        self.p = p
        self._debug = debug
        self._acc_ids = set([])
        self._balances = {}
        self._last_transfer_id = 0

    def _rcv_line(self):
        out = self.p.recvline()
        if self._debug:
            print 'BEGIN{}END'.format(out)
        return out

    def _unrcv_line(self, line):
        if self._debug:
            print 'UNRECV{}END'.format(line)
        self.p.unrecv(line)

    def _snd_line(self, line):
        if self._debug:
            print 'SND{}END'.format(line)
        self.p.sendline(line)

    def _get_acc_id(self):
        for i in xrange(1, 11):
            if i not in self._acc_ids:
                self._acc_ids.add(i)
                return i
        assert False, "Not enough account ids"

    def read_menu(self):
        line = ""
        while "8. Exit" not in line:
            line = self._rcv_line()

    def _read_until_menu(self):
        output = ""
        line = ""
        while "Menu:" not in line:
            line = self._rcv_line()
            if "Menu:" in line:
                self._unrcv_line(line)
            else:
                output += line
        return output

    def create_account(self):
        self.read_menu()
        self._snd_line("1")
        acc_id = self._get_acc_id()
        self._snd_line(str(acc_id))
        self._rcv_line()
        self._snd_line(str(1)) # account type
        self._rcv_line()
        self._snd_line("a") # account description
        self._rcv_line()
        self._snd_line("Y35") # printer
        self._balances[acc_id] = 0;
        return acc_id

    def edit_account(self, acc_id, desc):
        assert len(desc) < 110, "Description is too long"
        self.read_menu()
        self._snd_line("3")
        self._rcv_line()
        self._snd_line(str(acc_id))
        self._rcv_line()
        self._snd_line(desc)

    # this function transfers some amount of money from src to tgt so that tgt ends
    # up with final_amount as balance
    def create_transfer(self, src_id=1, tgt_id=1, final_amount=0, desc="", force=False):
        if self._balances[tgt_id] == final_amount and not force:
            return
        assert self._last_transfer_id < 64, "Transaction limit reached"
        self.read_menu()
        self._snd_line("5")
        self._rcv_line()
        self._snd_line(str(src_id))
        self._rcv_line()
        self._snd_line(str(tgt_id))
        self._rcv_line()
        amount = final_amount - self._balances[tgt_id]
        self._snd_line(str(amount))
        self._balances[src_id] -= amount
        self._balances[tgt_id] += amount
        self._rcv_line()
        self._snd_line(desc)
        tid = self._last_transfer_id
        self._last_transfer_id += 1
        return tid

    def cancel_transfer(self, tr_id):
        self.read_menu()
        self._snd_line("7")
        self._rcv_line()
        self._snd_line(str(tr_id))
        self._rcv_line()

    def print_account(self, acc_id, nowait=False):
        self.read_menu()
        self._snd_line("2")
        self._rcv_line()
        self._snd_line(str(acc_id))
        if not nowait:
            return self._read_until_menu()
        return ""

    def transfer_history(self):
        self.read_menu()
        self._snd_line("6")
        return self._read_until_menu()
